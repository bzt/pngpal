/*
 * pngpal/main.c
 *
 * Copyright (C) 2021 bzt (bztsrc@gitlab)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief Small utility to convert colours in images according to a palette
 *
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libpng/png.h"

#define STB_IMAGE_IMPLEMENTATION
#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STBI_NO_FAILURE_STRINGS
#include "stb_image.h"

/* command line options */
int verbose = 0, cie76 = 0, mc = 256, indexed = 1, ramps = 0;
/* palette to use, filled with GIMP Default palette */
uint32_t pal[257] = { 24, 0xff000000,0xff0000ff,0xffff00ff,0xffff0000,0xffffff00,0xff00ff00,0xff00ffff,0xff00007f,
    0xff7f007f,0xff7f0000,0xff7f7f00,0xff007f00,0xff007f82,0xff000000,0xff191919,0xff333333,0xff4c4c4c,0xff666666,
    0xff7f7f7f,0xff999999,0xffb2b2b2,0xffcccccc,0xffe5e5e5,0xffffffff };
char *comment = NULL;

/**
 * Load an image
 */
unsigned char *image_load(char *fn, int *w, int *h)
{
    FILE *f;
    stbi__context s;
    stbi__result_info ri;
    unsigned char *data = NULL;
    int c = 0, nf = 1;

    *w = *h = 0;
    f = stbi__fopen(fn, "rb");
    if(!f) return NULL;
    stbi__start_file(&s, f);
    if(stbi__gif_test(&s)) {
        data = stbi__load_gif_main(&s, NULL, w, h, &nf, &c, 4);
        if(data && *w > 0 && *h > 0 && nf > 1)
            *h *= nf;
    } else {
        data = stbi__load_main(&s, w, h, &c, 4, &ri, 8);
    }
    fclose(f);
    comment = stbi_comment();
    if(data && *w > 0 && *h > 0)
        return data;
    if(data) free(data);
    return NULL;
}

/**
 * Write image to file
 */
int image_save(uint8_t *p, int w, int h, char *fn)
{
    FILE *f;
    uint32_t *ptr = (uint32_t*)p, pal[256];
    uint8_t *data, *pal2 = (uint8_t*)&pal;
    png_color pngpal[256];
    png_byte pngtrn[256];
    png_structp png_ptr;
    png_infop info_ptr;
    png_bytep *rows;
    png_text texts[1] = { 0 };
    int i, j, nc = 0;

    if(!p || !fn || !*fn || w < 1 || h < 1) return 0;
    f = fopen(fn, "wb+");
    if(!f) { fprintf(stderr,"Unable to write %s\r\n", fn); exit(2); }
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if(!png_ptr) { fclose(f); return 0; }
    info_ptr = png_create_info_struct(png_ptr);
    if(!info_ptr) { png_destroy_write_struct(&png_ptr, NULL); fclose(f); return 0; }
    if(setjmp(png_jmpbuf(png_ptr))) { png_destroy_write_struct(&png_ptr, &info_ptr); fclose(f); return 0; }
    png_init_io(png_ptr, f);
    png_set_compression_level(png_ptr, 9);
    png_set_compression_mem_level(png_ptr, 5);
    png_set_filter(png_ptr, PNG_FILTER_TYPE_BASE, PNG_FILTER_VALUE_NONE);
    rows = (png_bytep*)malloc(h * sizeof(png_bytep));
    data = (uint8_t*)malloc(w * h);
    if(!rows || !data) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
    /* lets see if we can save this as an indexed image */
    if(indexed)
        for(i = 0; i < w * h; i++) {
            for(j = 0; j < nc && pal[j] != ptr[i]; j++);
            if(j >= nc) {
                if(nc == 256) { nc = -1; break; }
                pal[nc++] = ptr[i];
            }
            data[i] = j;
        }
    else
        nc = -1;
    if(nc != -1) {
        for(i = j = 0; i < nc; i++) {
            pngpal[i].red = pal2[i * 4 + 0];
            pngpal[i].green = pal2[i * 4 + 1];
            pngpal[i].blue = pal2[i * 4 + 2];
            pngtrn[i] = pal2[i * 4 + 3];
        }
        png_set_PLTE(png_ptr, info_ptr, pngpal, nc);
        png_set_tRNS(png_ptr, info_ptr, pngtrn, nc, NULL);
        for(i = 0; i < h; i++) rows[i] = data + i * w;
    } else
        for(i = 0; i < h; i++) rows[i] = p + i * w * 4;
    png_set_IHDR(png_ptr, info_ptr, w, h, 8, nc == -1 ? PNG_COLOR_TYPE_RGB_ALPHA : PNG_COLOR_TYPE_PALETTE,
        PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    if(comment && *comment) {
        texts[0].key = "Comment"; texts[0].text = comment;
        png_set_text(png_ptr, info_ptr, texts, 1);
    }
    png_write_info(png_ptr, info_ptr);
    png_write_image(png_ptr, rows);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    free(rows);
    free(data);
    fclose(f);
    return 1;
}

/**
 * Convert color from sRGB to LAB (needed by CIE76 color matching)
 */
void rgb2lab(unsigned char *rgb, float *L, float *A, float *B)
{
    float x, y, z, r, g, b;
    r = rgb[0] / 255.0; if(r > 0.04045) { r = __builtin_powf((r + 0.055) / 1.055, 2.4); } else { r /= 12.92; } r *= 100.0;
    g = rgb[1] / 255.0; if(g > 0.04045) { g = __builtin_powf((g + 0.055) / 1.055, 2.4); } else { g /= 12.92; } g *= 100.0;
    b = rgb[2] / 255.0; if(b > 0.04045) { b = __builtin_powf((b + 0.055) / 1.055, 2.4); } else { b /= 12.92; } b *= 100.0;
    x = (r*0.4124 + g*0.3576 + b*0.1805) /  95.047; if(x > 0.008856) x = __builtin_powf(x, 0.33333); else x = (7.787*x) + (16.0/116.0);
    y = (r*0.2126 + g*0.7152 + b*0.0722) / 100.000; if(y > 0.008856) y = __builtin_powf(y, 0.33333); else y = (7.787*y) + (16.0/116.0);
    z = (r*0.0193 + g*0.1192 + b*0.9505) / 108.883; if(z > 0.008856) z = __builtin_powf(z, 0.33333); else z = (7.787*z) + (16.0/116.0);
    *L = 116.0 * y - 16.0; *A = 500.0 * (x - y); *B = 200.0 * (y - z);
}

/**
 * Convert color from LAB to sRGB (needed by Adobe Palette readers)
 */
uint32_t lab2rgb(float L, float A, float B)
{
    float y = (L + 16.0) / 116.0;
    float x = A / 500.0 + y;
    float z = y - B / 200.0;
    float r, g, b;
    x = 0.95047 * ((x * x * x > 0.008856) ? x * x * x : (x - 16/116) / 7.787);
    y = 1.00000 * ((y * y * y > 0.008856) ? y * y * y : (y - 16/116) / 7.787);
    z = 1.08883 * ((z * z * z > 0.008856) ? z * z * z : (z - 16/116) / 7.787);
    r = x *  3.2406 + y * -1.5372 + z * -0.4986; r = (r > 0.0031308) ? (1.055 * __builtin_powf(r, 1/2.4) - 0.055) : 12.92 * r;
    g = x * -0.9689 + y *  1.8758 + z *  0.0415; g = (g > 0.0031308) ? (1.055 * __builtin_powf(g, 1/2.4) - 0.055) : 12.92 * g;
    b = x *  0.0557 + y * -0.2040 + z *  1.0570; b = (b > 0.0031308) ? (1.055 * __builtin_powf(b, 1/2.4) - 0.055) : 12.92 * b;
    if(r > 1.0) { r = 1.0; } if(r < 0.0) r = 0.0;
    if(g > 1.0) { g = 1.0; } if(g < 0.0) g = 0.0;
    if(b > 1.0) { b = 1.0; } if(b < 0.0) b = 0.0;
    return 0xFF000000 | (((int)(r * 255.0) & 0xFF) << 0) | (((int)(g * 255.0) & 0xFF) << 8) | (((int)(b * 255.0) & 0xFF) << 16);
}

/**
 * Color conversion sRGB to HSV (needed by palette ramps)
 */
void rgb2hsv(uint32_t c, int *h, int *s, int *v)
{
    int r = (int)(((uint8_t*)&c)[2]), g = (int)(((uint8_t*)&c)[1]), b = (int)(((uint8_t*)&c)[0]), m, d;

    m = r < g? r : g; if(b < m) m = b;
    *v = r > g? r : g; if(b > *v) *v = b;
    d = *v - m; *h = 0;
    if(!*v) { *s = 0; return; }
    *s = d * 255 / *v;
    if(!*s) return;

    if(r == *v) *h = 43*(g - b) / d;
    else if(g == *v) *h = 85 + 43*(b - r) / d;
    else *h = 171 + 43*(r - g) / d;
    if(*h < 0) *h += 256;
}

/**
 * Color conversion HSV to RGB (needed by Adobe Palette readers)
 */
uint32_t hsv2rgb(int a, int h, int s, int v)
{
    int i, f, p, q, t;
    uint32_t c = (a & 255) << 24;

    if(!s) { ((uint8_t*)&c)[2] = ((uint8_t*)&c)[1] = ((uint8_t*)&c)[0] = v; }
    else {
        if(h > 255) i = 0; else i = h / 43;
        f = (h - i * 43) * 6;
        p = (v * (255 - s) + 127) >> 8;
        q = (v * (255 - ((s * f + 127) >> 8)) + 127) >> 8;
        t = (v * (255 - ((s * (255 - f) + 127) >> 8)) + 127) >> 8;
        switch(i) {
            case 0:  ((uint8_t*)&c)[2] = v; ((uint8_t*)&c)[1] = t; ((uint8_t*)&c)[0] = p; break;
            case 1:  ((uint8_t*)&c)[2] = q; ((uint8_t*)&c)[1] = v; ((uint8_t*)&c)[0] = p; break;
            case 2:  ((uint8_t*)&c)[2] = p; ((uint8_t*)&c)[1] = v; ((uint8_t*)&c)[0] = t; break;
            case 3:  ((uint8_t*)&c)[2] = p; ((uint8_t*)&c)[1] = q; ((uint8_t*)&c)[0] = v; break;
            case 4:  ((uint8_t*)&c)[2] = t; ((uint8_t*)&c)[1] = p; ((uint8_t*)&c)[0] = v; break;
            default: ((uint8_t*)&c)[2] = v; ((uint8_t*)&c)[1] = p; ((uint8_t*)&c)[0] = q; break;
        }
    }
    return c;
}

/**
 * Add a color to color map
 */
void cpaladd(uint32_t *out, int r, int g, int b, int mc)
{
    int i, dr, dg, db, m, q;
    int64_t d, dm;
    unsigned char *pal = (unsigned char*)(&out[1]);

    for(q=1; q<=8; q++) {
        m=-1; dm=3*65536+1;
        for(i=0; (uint32_t)i<out[0]; i++) {
            if(r==pal[i*4+0] && g==pal[i*4+1] && b==pal[i*4+2]) return;
            if(r>>q==pal[i*4+0]>>q && g>>q==pal[i*4+1]>>q && b>>q==pal[i*4+2]>>q) {
                db = b - pal[i*4+2]; dg = g - pal[i*4+1]; dr = r - pal[i*4+0];
                d = dr*dr + dg*dg + db*db;
                if(d < dm) { dm = d; m = i; }
                if(!dm) break;
            }
        }
        if(dm>9+9+9 && i<mc) {
            pal[i*4+2] = b;
            pal[i*4+1] = g;
            pal[i*4+0] = r;
            out[0]++;
            return;
        }
        if(m>=0) {
            pal[m*4+2] = ((pal[m*4+2] + b) >> 1);
            pal[m*4+1] = ((pal[m*4+1] + g) >> 1);
            pal[m*4+0] = ((pal[m*4+0] + r) >> 1);
            return;
        }
    }
}

/**
 * Convert hex string to binary number. Use this lightning fast implementation
 * instead of the unbeliveably crap, slower than a pregnant snail sscanf...
 */
unsigned int gethex(char *ptr, int len)
{
    unsigned int ret = 0;
    for(;len--;ptr++) {
        if(*ptr>='0' && *ptr<='9') {          ret <<= 4; ret += (unsigned int)(*ptr-'0'); }
        else if(*ptr >= 'a' && *ptr <= 'f') { ret <<= 4; ret += (unsigned int)(*ptr-'a'+10); }
        else if(*ptr >= 'A' && *ptr <= 'F') { ret <<= 4; ret += (unsigned int)(*ptr-'A'+10); }
        else break;
    }
    return ret;
}

/**
 * Get palette from file
 */
void getpal(char *fn, uint32_t *out, int mc)
{
    FILE *f;
    char buf[16384], *s, *t;
    unsigned char *data = NULL, *d, *e;
    int i, w = 0, h = 0;
    float C,M,Y,K;
    uint32_t c;

    memset(out, 0, 257 * sizeof(uint32_t));
    if(mc < 2) mc = 2;
    if(mc > 256) mc = 256;
    f = fopen(fn, "rb");
    if(f) {
        memset(buf, 0, sizeof(buf));
        i = fread(buf, sizeof(buf) - 1, 1, f);
        fclose(f);
        /* Adobe Photoshop Color File, no magic to identify... */
        s = strrchr(fn, '.');
        if(s && !strcmp(s, ".aco")) {
            if(verbose) printf("getpal: ACO %s\r\n", fn);
            for(w = 0, data = (uint8_t*)buf + 4; w < (((uint8_t)buf[2] << 8) | (uint8_t)buf[3]) && w < 256 && out[0] < 256; w++) {
                c = 0;
                switch(data[1]) {
                    /* RGB color space */
                    case 0: c = 0xFF000000 | (data[2]) | (data[4] << 8) | (data[6] << 16); break;
                    /* HSV color space */
                    case 1: c = hsv2rgb(0xFF, data[2], data[4], data[6]); break;
                    /* CMYK */
                    case 2:
                        C = (float)((data[2] << 8) | data[3]) / 655.35; M = (float)((data[4] << 8) | data[5]) / 655.35;
                        Y = (float)((data[6] << 8) | data[7]) / 655.35; K = (float)((data[8] << 8) | data[9]) / 655.35;
                        c = 0xFF000000 | (((int)(2.55 * C * K) & 0xFF) << 0) | (((int)(2.55 * M * K) & 0xFF) << 8) |
                            (((int)(2.55 * Y * K) & 0xFF) << 16);
                    break;
                    /* LAB */
                    case 7:
                        c = lab2rgb((float)((data[2] << 8) | data[3]) / 10000.0,
                                    ((float)((data[4] << 8) | data[5]) + 12800.0) / 25600.0,
                                    ((float)((data[6] << 8) | data[7]) + 12800.0) / 25600.0);
                    break;
                    /* grayscale */
                    case 8:
                        h = ((int)(255.0 * (float)((data[2] << 8) | data[3]) / 10000.0)) & 0xFF;
                        c = 0xFF000000 | h | (h << 8) | (h << 16);
                    break;
                    /* wide CMYK */
                    case 9:
                        C = (float)((data[2] << 8) | data[3]) / 10000.0; M = (float)((data[4] << 8) | data[5]) / 10000.0;
                        Y = (float)((data[6] << 8) | data[7]) / 10000.0; K = (float)((data[8] << 8) | data[9]) / 10000.0;
                        c = 0xFF000000 | (((int)(255.0 * (1.0 - C) * (1.0 - K)) & 0xFF) << 0) |
                            (((int)(255.0 * (1.0 - M) * (1.0 - K)) & 0xFF) << 8) | (((int)(255.0 * (1.0 - Y) * (1.0 - K)) & 0xFF) << 16);
                    break;
                    default: if(verbose) printf("getpal: unsupported color space %02x\r\n", data[1]); break;
                }
                data += 10;
                if(c) {
                    for(i = 0; (uint32_t)i < out[0] && out[i + 1] != c; i++);
                    if((uint32_t)i >= out[0]) out[++out[0]] = c;
                    if(out[0] == (uint32_t)mc) break;
                }
                /* skip over name */
                if(buf[1] == 2) data += 4 + data[3] * 2;
            }
        } else
        /* Adobe Swatch Exchange */
        if(!memcmp(buf, "ASEF", 4)) {
            if(verbose) printf("getpal: ASEF %s\r\n", fn);
            data = (uint8_t*)buf;
            w = (data[10] << 16) | data[11]; /* number of blocks, we don't handle more than 65536 blocks */
            for(data += 12; w >= 0 && data < (uint8_t*)buf + sizeof(buf) && out[0] < 256; w--, data += h) {
                h = ((data[2] << 24) | (data[3] << 16) | (data[4] << 8) | data[5]) + 6;
                /* if this is a color block */
                if(data[0] == 0 && data[1] == 1) {
                    /* skip over name */
                    d = data + 12 + ((data[6] << 8) | data[7]) * 2;
                    c = 0;
                    switch(d[-4]) {
                        /* RGB color space */
                        case 'R':
                            e = (uint8_t*)&C; e[0] = d[3]; e[1] = d[2]; e[2] = d[1]; e[3] = d[0];
                            e = (uint8_t*)&M; e[0] = d[7]; e[1] = d[6]; e[2] = d[5]; e[3] = d[4];
                            e = (uint8_t*)&Y; e[0] = d[11]; e[1] = d[10]; e[2] = d[9]; e[3] = d[8];
                            c = 0xFF000000 | (((int)(255.0 * Y) & 0xFF) << 16) | (((int)(255.0 * M) & 0xFF) << 8) | ((int)(255.0 * C) & 0xFF);
                        break;
                        /* LAB */
                        case 'L':
                            e = (uint8_t*)&C; e[0] = d[3]; e[1] = d[2]; e[2] = d[1]; e[3] = d[0];
                            e = (uint8_t*)&M; e[0] = d[7]; e[1] = d[6]; e[2] = d[5]; e[3] = d[4];
                            e = (uint8_t*)&Y; e[0] = d[11]; e[1] = d[10]; e[2] = d[9]; e[3] = d[8];
                            c = lab2rgb(C, M, Y);
                        break;
                        /* CMYK */
                        case 'C':
                            e = (uint8_t*)&C; e[0] = d[3]; e[1] = d[2]; e[2] = d[1]; e[3] = d[0];
                            e = (uint8_t*)&M; e[0] = d[7]; e[1] = d[6]; e[2] = d[5]; e[3] = d[4];
                            e = (uint8_t*)&Y; e[0] = d[11]; e[1] = d[10]; e[2] = d[9]; e[3] = d[8];
                            e = (uint8_t*)&K; e[0] = d[15]; e[1] = d[14]; e[2] = d[13]; e[3] = d[12];
                            c = 0xFF000000 | (((int)(255.0 * (1.0 - C) * (1.0 - K)) & 0xFF) << 0) |
                                (((int)(255.0 * (1.0 - M) * (1.0 - K)) & 0xFF) << 8) | (((int)(255.0 * (1.0 - Y) * (1.0 - K)) & 0xFF) << 16);
                        break;
                        /* grayscale */
                        case 'G':
                            e = (uint8_t*)&C; e[0] = d[3]; e[1] = d[2]; e[2] = d[1]; e[3] = d[0];
                            c = 0xFF000000 | (((int)(255.0 * C) & 0xFF) << 16) | (((int)(255.0 * C) & 0xFF) << 8) | ((int)(255.0 * C) & 0xFF);
                        break;
                        default:
                            if(verbose) printf("getpal: unsupported color space '%c%c%c%c'\r\n", d[-4], d[-3], d[-2], d[-1]);
                        break;
                    }
                    if(c) {
                        for(i = 0; (uint32_t)i < out[0] && out[i + 1] != c; i++);
                        if((uint32_t)i >= out[0]) out[++out[0]] = c;
                        if(out[0] == (uint32_t)mc) break;
                    }
                }
            }
        } else
        /* JASC Microsoft Palette */
        if(!memcmp(buf, "JASC-PAL", 7)) {
            if(verbose) printf("getpal: JASC-PAL %s\r\n", fn);
            s = buf;
            while(*s && *s != '\r' && *s != '\n') { s++; } while(*s == '\r' || *s == '\n') { s++; } /* skip JASC-PAL */
            while(*s && *s != '\r' && *s != '\n') { s++; } while(*s == '\r' || *s == '\n') { s++; } /* skip 0100 */
            while(*s && *s != '\r' && *s != '\n') { s++; } while(*s == '\r' || *s == '\n') { s++; } /* skip 256 */
            goto readtxt;
        } else
        /* GIMP Palette */
        if(!memcmp(buf, "GIMP", 4)) {
            if(verbose) printf("getpal: GIMP Palette %s\r\n", fn);
            s = buf;
readtxt:    for(; *s && s < buf + sizeof(buf) && out[0] < 256; s++) {
                if(*s == '\r' || *s == '\n') continue;
                if(*s == ' ' || (*s >= '0' && *s <= '9')) {
                    while(*s && *s == ' ') s++;
                    c = (atoi(s) & 0xFF) | 0xFF000000;
                    while(*s && *s >= '0' && *s <= '9') s++;
                    while(*s && *s == ' ') s++;
                    c |= (atoi(s) & 0xFF) << 8;
                    while(*s && *s >= '0' && *s <= '9') s++;
                    while(*s && *s == ' ') s++;
                    c |= (atoi(s) & 0xFF) << 16;
                    for(i = 0; (uint32_t)i < out[0] && out[i + 1] != c; i++);
                    if((uint32_t)i >= out[0]) out[++out[0]] = c;
                    if(out[0] == (uint32_t)mc) break;
                }
                while(*s && *s != '\r' && *s != '\n') s++;
            }
        } else
        /* GIF with global color table */
        if(!memcmp(buf, "GIF8", 4) && (buf[0xA] & 0x80)) {
            if(verbose) printf("getpal: GIF %s\r\n", fn);
            out[0] = 1 << ((buf[0xA] & 3) + 1);
            if(out[0] > (uint32_t)mc) out[0] = mc;
            if(out[0] < (uint32_t)mc) mc = out[0];
            for(i = 0; i < mc; i++)
                out[i + 1] = buf[0xD + i * 3] | (buf[0xE + i * 3] << 8) | (buf[0xF + i * 3] << 16) | 0xFF000000;
        } else
        /* Paint.NET, no real magic either */
        if(buf[0] == ';' || (buf[0] >= '0' && buf[0] <= '9') || (buf[0] >= 'a' && buf[0] <= 'f') || (buf[0] >= 'A' && buf[0] <= 'F')) {
            for(; *s && s < buf + sizeof(buf) && out[0] < 256; s++) {
                if(*s == ';') { while(*s && *s != '\r' && *s != '\n') s++; }
                if(*s == ' ' || *s == '\r' || *s == '\n') continue;
                for(t = s; *t && *t != ' ' && *t != '\r' && *t != '\n'; t++);
                if(t - s == 8) { c = (gethex(s, 2) << 24); s += 2; } else c = 0xFF000000;
                c |= gethex(s, 2) | (gethex(s + 2, 2) << 8) | (gethex(s + 4, 2) << 16);
                while(*s && *s != '\r' && *s != '\n') s++;
                for(i = 0; (uint32_t)i < out[0] && out[i + 1] != c; i++);
                if((uint32_t)i >= out[0]) out[++out[0]] = c;
                if(out[0] == (uint32_t)mc) break;
            }
        } else
        /* we have to do it the hard way */
        if(buf[0] || buf[1] || buf[2] || buf[3]) {
            if((data = image_load(fn, &w, &h)) && w > 0 && h > 0) {
                if(verbose) printf("getpal: quantizing image %s %d x %d\r\n", fn, w, h);
                for(i = 0; i < w * h * 4 && out[0] < (uint32_t)mc; i += 4)
                    cpaladd(out, data[i], data[i + 1], data[i + 2], mc);
            }
            if(comment) { free(comment); comment = NULL; }
            if(data) free(data);
        }
        if(!out[0] && verbose) printf("getpal: unable to parse %s\r\n", fn);
    }
}

/**
 * Remap image to palette, using CIE76 method or sRGB distance for matching closest color
 */
void convpal(uint8_t *p, int w, int h, int num, uint32_t *pal)
{
    float l, a, b, labpal[256 * 3], d, dm;
    int i, dr, dg, db, m, x, y;
    int64_t di, dmi;
    uint8_t *P = (uint8_t*)pal;
    if(!p || num < 1 || !pal) return;
    if(verbose) printf("convpal: converting using %s distance\r\n", cie76 ? "CIE76" : "sRGB");
    if(cie76)
        for(y = 0; y < num; y++)
            rgb2lab((unsigned char*)&pal[y + 1], &labpal[y * 3], &labpal[y * 3 + 1], &labpal[y * 3 + 2]);
    for(y = 0; y < h; y++) {
        printf("\rConverting %3d%%", y * 100 / h);
        for(x = 0; x < w; x++, p += 4) {
            if(cie76)
                for(i = m = 0, dm = 1.8e+19; i < num && dm > 0.0; i++) {
                    rgb2lab(p, &l, &a, &b);
                    l -= labpal[i * 3 + 0]; a -= labpal[i * 3 + 1]; b -= labpal[i * 3 + 2];
                    d = l*l + a*a + b*b;
                    if(d < dm) { dm = d; m = i; }
                }
            else
                for(i = m = 0, dmi = 3*65536+1; i < num && dmi; i++) {
                    dr = p[0] - P[i*4+0]; dg = p[1] - P[i*4+1]; db = p[2] - P[i*4+2];
                    di = dr*dr + dg*dg + db*db;
                    if(di < dmi) { dmi = di; m = i; }
                }
            p[0] = P[m*4+0];
            p[1] = P[m*4+1];
            p[2] = P[m*4+2];
        }
    }
    printf("\rDone.           \r\n");
}

/**
 * Generate palette ramps
 */
int histsort(const void *a, const void *b) { return *((uint32_t*)b) - *((uint32_t*)a); }
int lumasort(const void *a, const void *b) {
    uint8_t *pa = (uint8_t*)a, *pb = (uint8_t*)b;
    return  (0.299 * (float)pb[4] * (float)pb[4] + 0.587 * (float)pb[5] * (float)pb[5] + 0.114 * (float)pb[6] * (float)pb[6]) >=
            (0.299 * (float)pa[4] * (float)pa[4] + 0.587 * (float)pa[5] * (float)pa[5] + 0.114 * (float)pa[6] * (float)pa[6]) ?
            1 : -1;
}
uint8_t *genramps(uint8_t *p, int *wi, int *he)
{
    int i, j, k, z, h = 0, s = 0, v = 0, hidx[256];
    uint32_t *hist[256], *ret = NULL, c;
    uint8_t *pb = (uint8_t*)&pal[1];

    memset(pal, 0, sizeof(pal));
    for(i = 0; i < *wi * *he * 4 && pal[0] < 256; i += 4)
        cpaladd(pal, p[i], p[i + 1], p[i + 2], 256);
    free(p);
    if(!pal[0]) { *wi = *he = 0; return NULL; }
    memset(hist, 0, sizeof(hist));
    for(i = 0; (uint32_t)i < pal[0]; i++, pb += 4) {
        /* rows = hue; coloumns = value, reverse saturation */
        rgb2hsv(pal[i + 1], &h, &s, &v); k = (v << 8) | (255 - s);
        if(!hist[h]) {
            hist[h] = (uint32_t*)malloc(3 * sizeof(int));
            if(!hist[h]) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
            hist[h][0] = 1; hist[h][1] = k; hist[h][2] = pal[i + 1];
        } else {
            hist[h] = (uint32_t*)realloc(hist[h], (hist[h][0] * 2 + 3) * sizeof(uint32_t));
            if(!hist[h]) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
            hist[h][hist[h][0] * 2 + 1] = k; hist[h][hist[h][0] * 2 + 2] = pal[i + 1]; hist[h][0]++;
        }
    }
    for(i = 0; i < 256; i++)
        if(hist[i]) qsort(&hist[i][1], hist[i][0], 2 * sizeof(int), histsort);
    /* try to make the ramps less fragmented, merge darker small ramps into brighter ones */
    do {
        for(i = z = v = 0; i < 256; i++) { hidx[i] = 0; if(hist[i]) hidx[z++] = i; }
        for(i = 0; i < z; i++) {
            if((!i || hist[hidx[i - 1]][1] >= hist[hidx[i]][1]) && (i == z - 1 || hist[hidx[i + 1]][1] >= hist[hidx[i]][1])) {
                if(!i) { k = hidx[i + 1]; h = s = hidx[i + 1] - hidx[i]; } else
                if(i == z - 1) { k = hidx[i - 1]; h = s = hidx[i] - hidx[i - 1]; } else {
                    h = hidx[i] - hidx[i - 1]; s = hidx[i + 1] - hidx[i];
                    k = h < s ? hidx[i - 1] : hidx[i + 1];
                }
                if(hist[hidx[i]][0] > 3 && (h < s ? h : s) > 9) continue;
                hist[k] = (uint32_t*)realloc(hist[k], ((hist[k][0] + hist[hidx[i]][0]) * 2 + 1) * sizeof(uint32_t));
                if(!hist[k]) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
                memcpy(&hist[k][1 + hist[k][0] * 2], &hist[hidx[i]][1], hist[hidx[i]][0] * 2 * sizeof(uint32_t));
                hist[k][0] += hist[hidx[i]][0];
                free(hist[hidx[i]]); hist[hidx[i]] = NULL;
                v = 1; break;
            }
        }
    } while(v);
    for(i = s = v = 0; i < 256; i++)
        if(hist[i]) {
            s++; if(hist[i][0] > (uint32_t)v) v = hist[i][0];
            qsort(&hist[i][1], hist[i][0], 2 * sizeof(int), lumasort);
        }
    z = s < 32 && v < 32 ? 16 : (s < 64 && v < 64 ? 8 : 4);
    *wi = v * z; *he = s * z;
    ret = (uint32_t*)malloc((*wi) * (*he) * 4);
    if(!ret) { fprintf(stderr,"Not enough memory\r\n"); exit(1); }
    memset(ret, 0, (*wi) * (*he) * 4);
    for(i = h = 0; i < 256; i++)
        if(hist[i]) {
            for(v = 0; (uint32_t)v < hist[i][0]; v++) {
                c = hist[i][(v + 1) * 2] | 0xFF000000;
                for(j = 0; j < z; j++)
                    for(k = 0; k < z; k++)
                        ret[*wi * (z * h + j) + z * v + k] = c;
            }
            free(hist[i]);
            h++;
        }
    printf("\rDone.           \r\n");
    return (uint8_t*)ret;
}

/**
 * Usage instructions
 */
void usage()
{
    printf("pngpal by bzt Copyright (C) 2021 MIT license\r\n https://gitlab.com/bztsrc/pngpal\r\n\r\n");
    printf("./pngpal [-m <num>] [-p <palfile>] [-c] [-t] [-g] <input image> <output png>\r\n\r\n");
    printf(" -m <num>       set the maximum number of colours\r\n");
    printf(" -p <palfile>   load palette (aco, ase, jasc-pal, gpl, paint.net, any image)\r\n");
    printf(" -c             use CIE76 method instead of sRGB distance\r\n");
    printf(" -t             make the output a truecolor image\r\n");
    printf(" -g             generate palette ramps\r\n");
    printf(" <input image>  input image (png, jpg, gif, tga, bmp, pnm, psd)\r\n");
    printf(" <output png>   output image, always png (truecolor or indexed if possible)\r\n\r\n");
    exit(0);
}

/**
 * Main function
 */
int main(int argc, char **argv)
{
    uint8_t *p = NULL;
    char *in = NULL, *out = NULL;
    int i, w = 0, h = 0;

    /* parse command line */
    for(i = 1; i < argc; i++)
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case 'm': mc = atoi(argv[++i]); break;
                case 'p': getpal(argv[++i], pal, mc); break;
                case 'v': verbose++; break;
                case 'c': cie76++; break;
                case 't': indexed = 0; break;
                case 'g': ramps++; break;
            }
        } else {
            if(!in) in = argv[i];
            else out = argv[i];
        }
    if(!in || !out) usage();
    /* load input */
    p = image_load(in, &w, &h);
    if(!p) { fprintf(stderr,"Unable to load %s\r\n", in); exit(2); }
    /* convert colours */
    if(ramps)
        p = genramps(p, &w, &h);
    else
        convpal(p, w, h, pal[0], &pal[1]);
    /* save result image */
    image_save(p, w, h, out);
    return 0;
}
